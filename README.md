# Tools
My own scripts, snippets, dot files, etc...

## General
* `apktool` => download & "drag'n'drop" your file (decompile) or folder (recompile) into it.
* `curl.sh` => download & launch, set your variables (`URL` for your url & `OUTPUT` for your output folder & or file)
* `cadoles.sh` => reset a linux ubuntu laptop to a known state.

## Auto

### Config

#### Server
* Connect to your server as root via SSH => `ssh root@your.server`
* Download & run installation script => `curl -sSL https://gitlab.com/kd-works/scripts/raw/main/unix/auto/config/server/install.sh | bash`

### Install

#### Apps

##### ProcessMaker
* Connect to your server as root via SSH => `ssh root@your.server`
* Download & run installation script => `curl -sSL https://gitlab.com/kd-works/scripts/raw/main/unix/auto/install/apps/processmaker/install.sh | bash`

##### Pterodactyl
* Connect to your server as root via SSH => `ssh root@your.server`
* Download & run installation script => `curl -sSL https://gitlab.com/kd-works/scripts/raw/main/unix/auto/install/apps/pterodactyl/install.sh | bash`
