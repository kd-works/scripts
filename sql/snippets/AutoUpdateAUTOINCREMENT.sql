SET @num := 0;

UPDATE `table` SET id = @num := (@num + 1);

ALTER TABLE `table` AUTO_INCREMENT = 1;
