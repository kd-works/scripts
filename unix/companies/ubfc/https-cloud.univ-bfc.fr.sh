#!/bin/bash

echo -ne "\033]2;HTTPS Verification Tool - cloud.univ-bfc.fr\007"

while true; do
	res=`curl -s "http://cloud.univ-bfc.fr" | grep "301 Moved*" | wc -l`

	if [[ $res == "1" ]]; then
		echo -e " \033[31mHTTP\033[0m "
	else
		echo -e " \033[32mHTTPS\033[0m "
	fi
done
