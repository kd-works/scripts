#!/bin/bash

if [ "$(whoami)" != "root" ]; then
  SUDO=sudo
fi

curl -sSL https://gitlab.com/kd-works/scripts/raw/main/unix/auto/install/apps/processmaker/install.sh | $SUDO bash
