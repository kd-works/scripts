#!/bin/bash
# ProcessMaker installation wrapper
# https://www.processmaker.com

# Am I root?
if [ "x$(id -u)" != 'x0' ]; then
  echo 'Error: this script can only be executed by root'
  exit 1
fi

echo "Debian 8 Installer"

read -p "PROCESSMAKER VERSION (latest, 3.3.0, etc...): " VERSION_PROCESSMAKER

if [ $VERSION_PROCESSMAKER = "latest" ]; then
  curl -sSL "https://sourceforge.net/projects/processmaker/files/latest/download" -o "processmaker-$VERSION_PROCESSMAKER-community.tar.gz"
else
  curl -sSL "https://sourceforge.net/projects/processmaker/files/ProcessMaker/$VERSION_PROCESSMAKER/processmaker-$VERSION_PROCESSMAKER-community.tar.gz/download" -o "processmaker-community.tar.gz"
fi
