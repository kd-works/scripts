#!/bin/bash

if [ "$(whoami)" != "root" ]; then
    SUDO=sudo
fi

APT=${SUDO} apt -y
INSTALL=${APT} install

echo "SETUP SERVER"

server:update() {
    ${APT} update
    ${APT} upgrade
    ${APT} autoremove
}

server() {
    server:update
}

server

echo "Done!"
read -n 1 -s
