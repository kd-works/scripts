#!/bin/bash

if [ "$(whoami)" != "root" ]; then
    SUDO=sudo
fi

APT=${SUDO} apt -y
INSTALL=${APT} install

echo "SETUP"

read -p 'Domain Name (pm.example.com): ' DOMAIN_NAME

##############################

#!/bin/bash

apt update && apt upgrade -y

## Tools
apt install -y curl apt-transport-https lsb-release ca-certificates dirmngr

## phpMyAdmin
apt install phpmyadmin
ln -s /etc/phpmyadmin/apache.conf /etc/apache2/sites-enabled/phpmyadmin.conf
/etc/init.d/apache2 restart

## Configure PHP
nano /etc/php5/apache2/php.ini
"""
memory_limit = 128M

file_uploads = On
short_open_tag = On

post_max_size = 24M 
upload_max_filesize = 24M

date.timezone = America/New_York

error_reporting = E_ALL & ~E_DEPRECATED & ~E_STRICT
display_errors = Off
"""

## Download
mkdir /opt/processmaker && cd /opt/processmaker
wget https://sourceforge.net/projects/processmaker/files/ProcessMaker/3.2.1/processmaker-3.2.1-community.tar.gz/download processmaker-3.2.1-community.tar.gz
tar -C /opt -xzvf processmaker-3.2.1-community.tar.gz
ls .
"""
components  gulliver  LICENSE.txt      processmaker      rbac    vendor
framework   INSTALL.txt  pmos.conf.example  processmaker.bat  shared  workflow
"""
chmod -R 770 shared workflow/public_html gulliver/js gulliver/thirdparty/html2ps_pdf/cache
cd workflow/engine
chmod -R 770 config content/languages plugins xmlform js/labels
chown -R www-data:www-data ../

## MySQL
mysql -u root -p
mysql> GRANT ALL ON *.* TO 'USER'@'localhost' IDENTIFIED BY 'PASSWORD' WITH GRANT OPTION;
mysql> exit;

## Service
service mysql start
update-rc.d mysql defaults

## Configuration of Apache
cp /opt/processmaker/pmos.conf.example /etc/apache2/sites-available/pmos.conf
nano /etc/apache2/sites-available/pmos.conf
"""
# Please change the IP address with your server IP address and
# the ServerName with your own subdomains.
NameVirtualHost your_ip_address

#processmaker virtual host
<VirtualHost your_ip_address >
   ServerName "your_processmaker_domain"
   DocumentRoot /opt/processmaker/workflow/public_html

   <Directory /opt/processmaker/workflow/public_html>
      AddDefaultCharset UTF-8
      AllowOverRide All
      Options FollowSymlinks
      Order allow,deny
      Allow from all

      RewriteEngine On
         RewriteCond %{REQUEST_FILENAME} !-f
         RewriteRule ^(.*)$ /app.php [QSA,L]

      ExpiresActive On
      ExpiresDefault "access plus 1 day"
      ExpiresByType image/gif "access plus 1 day"
      ExpiresByType image/png "access plus 1 day"
      ExpiresByType image/jpg "access plus 1 day"
      ExpiresByType text/css "access plus 1 day"
      ExpiresByType text/javascript "access plus 1 day"

      #Deflate filter is optional. It reduces download size, but adds slightly more CPU processing:
      AddOutputFilterByType DEFLATE text/html
   </Directory>
</VirtualHost>
"""

## Apache Enable site + Modules
a2ensite pmos.conf
ls /etc/apache2/mods-enabled/
"""
a2enmod expires
a2enmod rewrite 
a2enmod deflate
a2enmod vhost_alias
"""
a2enmod filter

# remove line from pmos.conf
AddOutputFilterByType DEFLATE text/html
# remove line

## Apache
nano /etc/apache2/apache2.conf
"""
ServerName ip-address

ServerName 127.0.0.1

Listen ip-address:port

Listen 127.0.0.1:8080

Listen 8080
"""
netstat -tanp

## Disable the default site
a2dissite default
/etc/init.d/apache2 reload
a2dissite 000-default.conf
service apache2 reload
