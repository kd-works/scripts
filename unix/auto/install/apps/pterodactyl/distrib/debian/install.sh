#!/bin/bash

if [ "$(whoami)" != "root" ]; then
    SUDO=sudo
fi

APT=${SUDO} apt -y

read -p 'domain name (panel.example.com): ' domain
echo "PANEL SETUP"

${APT} update
${APT} upgrade
${APT} autoremove

${APT} install software-properties-common apt-transport-https lsb-release curl dirmngr ca-certificates gnupg2 unzip git tar nginx redis-server

curl -sS https://packages.sury.org/php/README.txt | ${SUDO} bash

${APT} install php7.2 php7.2-cli php7.2-gd php7.2-mysql php7.2-pdo php7.2-mbstring php7.2-tokenizer php7.2-bcmath php7.2-xml php7.2-fpm php7.2-curl php7.2-zip

${APT} purge apache*
${APT} remove apache*

${APT} autoremove

curl -sS https://downloads.mariadb.com/MariaDB/mariadb_repo_setup | ${SUDO} bash

${APT} install mariadb-server

curl -sS https://getcomposer.org/installer | ${SUDO} php -- --install-dir=/usr/local/bin --filename=composer

${SUDO} mkdir -p /var/www/pterodactyl
cd /var/www/pterodactyl

curl -L https://github.com/pterodactyl/panel/releases/download/v0.7.13/panel.tar.gz | ${SUDO} tar --strip-components=1 -xzv
${SUDO} chmod -R 755 storage/* bootstrap/cache/

${SUDO} cp .env.example .env
${SUDO} composer install --no-dev --optimize-autoloader

clear
echo -e ""
echo -e "! copy and don't lose it !"
php artisan key:generate --force
read -n 1 -s

clear
echo -e "USE mysql;"
echo -e "CREATE USER 'pterodactyl'@'127.0.0.1' IDENTIFIED BY 'lytcadoretp';"
echo -e "CREATE DATABASE panel;"
echo -e "GRANT ALL PRIVILEGES ON panel.* TO 'pterodactyl'@'127.0.0.1' WITH GRANT OPTION;"
echo -e "FLUSH PRIVILEGES;"
read -n 1 -s
mysql -u root -p

php artisan p:environment:setup
php artisan p:environment:database
php artisan p:environment:mail
php artisan migrate --seed
php artisan p:user:make
chown -R www-data:www-data *

clear
echo -e "! copy the line below !"
echo -e "! then paste it in the crontab interface !"
echo -e "* * * * * php /var/www/pterodactyl/artisan schedule:run >> /dev/null 2>&1"
read -n 1 -s
${SUDO} crontab -e

echo -e "# Pterodactyl Queue Worker File\n# ----------------------------------\n\n[Unit]\nDescription=Pterodactyl Queue Worker\nAfter=redis-server.service\n\n[Service]\n# On some systems the user and group might be different.\n# Some systems use 'apache' as the user and group.\nUser=www-data\nGroup=www-data\nRestart=always\nExecStart=/usr/bin/php /var/www/pterodactyl/artisan queue:work --queue=high,standard,low --sleep=3 --tries=3\n\n[Install]\nWantedBy=multi-user.target" > /etc/systemd/system/pteroq.service
${SUDO} systemctl enable pteroq.service
${SUDO} systemctl start pteroq

${APT} install letsencrypt
${SUDO} service nginx stop
${SUDO} letsencrypt certonly -d $domain

echo -e 'server_tokens off;\n\nserver {\n    listen 80;\n    server_name '$domain';\n    return 301 https://$server_name$request_uri;\n}\n\nserver {\n    listen 443 ssl http2;\n    server_name '$domain';\n\n    root /var/www/pterodactyl/public;\n    index index.php;\n\n    access_log /var/log/nginx/pterodactyl.app-access.log;\n    error_log  /var/log/nginx/pterodactyl.app-error.log error;\n\n    # allow larger file uploads and longer script runtimes\n    client_max_body_size 100m;\n    client_body_timeout 120s;\n\n    sendfile off;\n\n    # SSL Configuration\n    ssl_certificate /etc/letsencrypt/live/'$domain'/fullchain.pem;\n    ssl_certificate_key /etc/letsencrypt/live/'$domain'/privkey.pem;\n    ssl_session_cache shared:SSL:10m;\n    ssl_protocols TLSv1.2;\n    ssl_ciphers "ECDHE-ECDSA-AES256-GCM-SHA384:ECDHE-RSA-AES256-GCM-SHA384:ECDHE-ECDSA-CHACHA20-POLY1305:ECDHE-RSA-CHACHA20-POLY1305:ECDHE-ECDSA-AES128-GCM-SHA256:ECDHE-RSA-AES128-GCM-SHA256:ECDHE-ECDSA-AES256-SHA384:ECDHE-RSA-AES256-SHA384:ECDHE-ECDSA-AES128-SHA256:ECDHE-RSA-AES128-SHA256";\n    ssl_prefer_server_ciphers on;\n\n    # See https://hstspreload.org/ before uncommenting the line below.\n    # add_header Strict-Transport-Security "max-age=15768000; preload;";\n    add_header X-Content-Type-Options nosniff;\n    add_header X-XSS-Protection "1; mode=block";\n    add_header X-Robots-Tag none;\n    add_header Content-Security-Policy "frame-ancestors '\''self'\''";\n    add_header X-Frame-Options DENY;\n    add_header Referrer-Policy same-origin;\n\n    location / {\n        try_files $uri $uri/ /index.php?$query_string;\n    }\n\n    location ~ \.php$ {\n        fastcgi_split_path_info ^(.+\.php)(/.+)$;\n        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;\n        fastcgi_index index.php;\n        include fastcgi_params;\n        fastcgi_param PHP_VALUE "upload_max_filesize = 100M \n post_max_size=100M";\n        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;\n        fastcgi_param HTTP_PROXY "";\n        fastcgi_intercept_errors off;\n        fastcgi_buffer_size 16k;\n        fastcgi_buffers 4 16k;\n        fastcgi_connect_timeout 300;\n        fastcgi_send_timeout 300;\n        fastcgi_read_timeout 300;\n        include /etc/nginx/fastcgi_params;\n    }\n\n    location ~ /\.ht {\n        deny all;\n    }\n}' > /etc/nginx/sites-available/pterodactyl.conf
rm /etc/nginx/sites-enabled/default
ln -s /etc/nginx/sites-available/pterodactyl.conf /etc/nginx/sites-enabled/pterodactyl.conf
service nginx start
clear

echo "DAEMON SETUP"

curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt update
apt -y upgrade
apt -y install docker-ce
systemctl enable docker

curl -sL https://deb.nodesource.com/setup_8.x | bash -
apt -y install nodejs make gcc g++
npm i -g npm@latest

curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
add-apt-repository "deb https://dl.yarnpkg.com/debian/ stable main"
apt update
apt -y install yarn

mkdir -p /srv/daemon /srv/daemon-data
cd /srv/daemon

curl -L https://github.com/pterodactyl/daemon/releases/download/v0.6.12/daemon.tar.gz | tar --strip-components=1 -xzv
npm install --only=production

clear
echo -e ""
echo "! configure node on the web !"
echo -e "! enter the token of your node !"
read -p 'Token: ' token
clear
npm run configure -- --panel-url https://$domain --token $token
npm start

echo -e "[Unit]\nDescription=Pterodactyl Wings Daemon\nAfter=docker.service\n\n[Service]\nUser=root\n#Group=some_group\nWorkingDirectory=/srv/daemon\nLimitNOFILE=4096\nPIDFile=/var/run/wings/daemon.pid\nExecStart=/usr/bin/node /srv/daemon/src/index.js\nRestart=on-failure\nStartLimitInterval=600\n\n[Install]\nWantedBy=multi-user.target" > /etc/systemd/system/wings.service
systemctl daemon-reload
systemctl enable wings
systemctl start wings

echo "done!"
read -n 1 -s
reboot