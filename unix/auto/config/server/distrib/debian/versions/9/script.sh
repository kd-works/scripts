#!/bin/bash

MY_IP="xx.xx.xx.xx"
MY_EMAIL="email@domain.tld"

SENDER_EMAIL="sender<email@domain.tld>"

SERVER_PORT="22"

USER_NAME="user"
USER_PASS="password"

init() {
    apt update
    apt -y upgrade
    apt -y autoremove
    
    apt -y install curl
}

setup:user() {
    # TODO - update the method
    adduser $USER_NAME --quiet
    usermod -aG sudo $USER_NAME
    passwd -q $USER_NAME $USER_PASS
}

setup:ssh() {
    sed -i "s+#Port 22+Port $SERVER_PORT+g" /etc/ssh/sshd_config
    sed -i "s+#PermitRootLogin yes+PermitRootLogin no+g" /etc/ssh/sshd_config
    sed -i "s+#PasswordAuthentication yes+PasswordAuthentication no+g" /etc/ssh/sshd_config
    sed -i "s+#PermitEmptyPasswords no+PermitEmptyPasswords no+g" /etc/ssh/sshd_config
    
    mkdir -p /home/$USER_NAME/.ssh
    cp /root/.ssh/authorized_keys /home/$USER_NAME/.ssh/
    chown -R $USER_NAME:$USER_NAME /home/$USER_NAME/.ssh
    
    service ssh restart
}

setup:fail2ban() {
    apt -y install fail2ban
    
    sed -i "s+ignoreip = 127.0.0.1\/8+ignoreip = 127.0.0.1\/8 $MY_IP+g" /etc/fail2ban/jail.conf
    
    service fail2ban restart
}

setup:portsentry() {
    apt -y install portsentry
    
    #sed -i "s+ignoreip = 127.0.0.1\/8+ignoreip = 127.0.0.1\/8 $MY_IP+g" /etc/fail2ban/jail.conf
    
    service portsentry restart
}

setup:login-notify() {
    FILE_NAME="login-notify.sh"
    FILE_PATH="/etc/ssh/$FILE_NAME"

    apt -y install mailutils sendmail

    echo -e '#!/bin/bash'"\n\nsender=\"$SENDER_EMAIL\"\nrecepient=\"$MY_EMAIL\"\n\nif [ \"\$PAM_TYPE\" != \"close_session\" ]; then\n    host=\"\`hostname\`\"\n    ip=\`echo \$SSH_CONNECTION | cut -d \" \" -f 1\`\n\n    subject=\"SSH Login: \$PAM_USER from \$PAM_RHOST on \$host\"\n    message=\"User \$USER just logged in from \$ip/\$PAM_RHOST\\\n\\\n\`env\`\"\n\n    echo \"\$message\" | mail -s \"\$subject\" -r \"\$sender\" \"\$recepient\"\nfi" > $FILE_PATH
    chmod +x $FILE_PATH
    sed -i "\$asession optional pam_exec.so seteuid $FILE_PATH" /etc/pam.d/sshd
}

init

setup:user
setup:ssh
setup:fail2ban
setup:portsentry
setup:login-notify

reboot
