#!/bin/sh

setup:ssh() {
    sed -i "s+#Port 22+Port $SERVER_PORT+g" /etc/ssh/sshd_config
    sed -i "s+#PermitRootLogin yes+PermitRootLogin no+g" /etc/ssh/sshd_config
    sed -i "s+#PasswordAuthentication yes+PasswordAuthentication no+g" /etc/ssh/sshd_config
    sed -i "s+#PermitEmptyPasswords no+PermitEmptyPasswords no+g" /etc/ssh/sshd_config
    
    mkdir -p /home/$USER_NAME/.ssh
    cp /root/.ssh/authorized_keys /home/$USER_NAME/.ssh/
    chown -R $USER_NAME:$USER_NAME /home/$USER_NAME/.ssh
    
    service ssh restart
}
