#!/bin/bash

# CurseForge Modpacks Download & Installation Script
# Author: Kasai. <me@kasai.moe> (https://kasai.moe)

## Init (modpack)
MODPACK_SLUG="ftb-sky-adventures"
MODPACK_VERSION="latest" # "latest" or "1.0.0"
SERVER_JARFILE="server.jar"

## Init (settings)
SETTINGS_FILENAME="$0"
SETTINGS_TMP_FOLDER="tmp_data"
SETTINGS_FORGE_INSTALL_LOG="installer.jar.log"
SETTINGS_PROD=true
SETTINGS_DEBUG=false

## Set URLs
URL_CURSEFORGE="https://www.curseforge.com/minecraft/modpacks"
URL_MINECRAFTFORGE="https://files.minecraftforge.net/maven/net/minecraftforge/forge"
URL_MODPACK="$URL_CURSEFORGE/$MODPACK_SLUG"

## Archive usefull content (we don't want these to be deleted!)
MODPACK_ARCHIVE_CONTENT="world mods config libraries scripts resources schematics questpacks animation server.properties server-icon.png perms.txt"

## Utils Functions
req() {
  curl -sSL $1 -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept-Language: en-US,en;q=0.5' --compressed $2 $3 $4
  [ $SETTINGS_DEBUG = true ] && echo curl -sSL $1 -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; rv:78.0) Gecko/20100101 Firefox/78.0' -H 'Accept-Language: en-US,en;q=0.5' --compressed $2 $3 $4
}

## Verify if the var $MODPACK_VERSION is set to "LATEST", if so re-set it the the actual latest version
if [ -z "$MODPACK_VERSION" ] || [ "$MODPACK_VERSION" = "latest" ]; then
  TMP_MODPACK_VERSION_SLUG=$(req -sL $URL_MODPACK | grep -A20 Recent | grep -oe "/[0-9][0-9][0-9][0-9][0-9][0-9][0-9]\"" | tail -c +2 | sed 's/.$//')
  [ $SETTINGS_DEBUG = true ] && echo "\$TMP_MODPACK_VERSION_SLUG: $TMP_MODPACK_VERSION_SLUG"

  MODPACK_VERSION=$(req -sL $URL_MODPACK | grep -A5 $TMP_MODPACK_VERSION_SLUG | grep -oe "[0-9]\.[0-9]\?\.\?[0-9]\?\.\?[0-9]" | sed -n 1p)
  [ $SETTINGS_DEBUG = true ] && echo "\$MODPACK_VERSION: $MODPACK_VERSION"
fi

## Modpack Part
MODPACK_VERSION_SLUG=$(req -sL $URL_MODPACK/files/all | grep -B4 "\<$MODPACK_VERSION\>" | grep -oe "[0-9][0-9][0-9][0-9][0-9][0-9][0-9]")
[ $SETTINGS_DEBUG = true ] && echo "\$MODPACK_VERSION_SLUG: $MODPACK_VERSION_SLUG"

MODPACK_VERSION_SERVER_SLUG=$(req -sL $URL_MODPACK/files/$MODPACK_VERSION_SLUG | grep Server | grep -oe "/[0-9][0-9][0-9][0-9][0-9][0-9][0-9]" | sed -n 1p | tail -c +2)
[ $SETTINGS_DEBUG = true ] && echo "\$MODPACK_VERSION_SERVER_SLUG: $MODPACK_VERSION_SERVER_SLUG"

MODPACK_VERSION_MC=$(req -sL $URL_MODPACK/files/$MODPACK_VERSION_SLUG | grep -A3 "Supported Minecraft" | grep -oe "[0-9]\.[0-9]\?[0-9]\?\.\?[0-9]\?[0-9]" | sed -n 2p)
[ $SETTINGS_DEBUG = true ] && echo "\$MODPACK_VERSION_MC: $MODPACK_VERSION_MC"

MODPACK_AUTHOR=$(req -sL $URL_MODPACK/files/$MODPACK_VERSION_SLUG | grep -A5 "Uploaded by" | grep -oe "/[a-zA-Z0-9]*\"" | tail -c +2 | sed 's/.$//')
[ $SETTINGS_DEBUG = true ] && echo "\$MODPACK_AUTHOR: $MODPACK_AUTHOR"

# problem when $MODPACK_VERSION_SERVER_SLUG give us 2 versions like for the modpack "all-the-mods-3"
#VARISARRAY=$({ typeset -p $TAB | grep -qP '(declare|typeset) -a'; } && echo "yes" || echo "no")

# TODO
#
# should we display if a modpack's release is alpha/beta/release?
#
# trying to "get" all of the mods through the webpage then download them 1 by 1 (file "manifest.json" in archive)
# am i crazy?
if [ -z "$MODPACK_VERSION_SERVER_SLUG" ]; then
  # Send message saying to change the version to an upper one 'cause this one don't have a "server" archive file
  echo "This version ($MODPACK_VERSION) of the modpack \"$MODPACK_SLUG\" for MC$MODPACK_VERSION_MC by $MODPACK_AUTHOR don't have a server-pack!"
  echo "Please upgrade to a newer one!"

  # Force exit the program
  [ $SETTINGS_PROD = true ] && exit $?

  MODPACK_MODSLIST=$(req -sL $URL_MODPACK/files/$MODPACK_VERSION_SLUG | grep "\<Include\>" -A20 | grep "/projects/" | grep -oe "[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]")
  MODPACK_MODSLIST_VERSION_MC=$(echo $MODPACK_VERSION_MC | grep -oe "[0-9].[0-9]\?[0-9]")

  for el in $MODPACK_MODSLIST; do
    MODPACK_MODSLIST_MOD_SLUG=$(req -sL $URL_CURSEFORGE/$el | grep "\<Recent Files\>" -A5 | grep -oe "projects/[a-z0-9-]*/" | tail -c +10 | sed 's/.$//')

    MODPACK_MODSLIST_MOD_MCVERSION_GAMEVERSION_1=$(req -sL $URL_CURSEFORGE/$MODPACK_MODSLIST_MOD_SLUG | grep "\<Recent Files\>" -A400 | grep "\<Minecraft $MODPACK_MODSLIST_VERSION_MC\>" -B1 | grep -oe "[0-9][0-9][0-9][0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]")
    MODPACK_MODSLIST_MOD_MCVERSION_GAMEVERSION_2=$(req -sL $URL_CURSEFORGE/$MODPACK_MODSLIST_MOD_SLUG | grep "\<Recent Files\>" -A400 | grep "\<Minecraft $MODPACK_MODSLIST_VERSION_MC\>" -B1 | grep -oe "%3a[0-9][0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]")
    MODPACK_MODSLIST_MOD_MCVERSION_WEBPAGE=$(req -sL "$URL_CURSEFORGE/$MODPACK_MODSLIST_MOD_SLUG/files?filter-game-version=$MODPACK_MODSLIST_MOD_MCVERSION_GAMEVERSION_1$MODPACK_MODSLIST_MOD_MCVERSION_GAMEVERSION_2")

    MODPACK_MODSLIST_MOD_LATESTFILE=$(echo $MODPACK_MODSLIST_MOD_MCVERSION_WEBPAGE | grep "/download" | sed -n 1p | grep -oe "[0-9][0-9][0-9]\?[0-9]\?[0-9]\?[0-9]\?[0-9]")

    MODPACK_MODSLIST_MOD_FILENAME=$(req -sL $URL_CURSEFORGE/$MODPACK_MODSLIST_MOD_SLUG/files/$MODPACK_MODSLIST_MOD_LATESTFILE | grep "\<Filename\>" -A1 | grep -oe "[A-Za-z0-9.-]*.jar")
    MODPACK_MODSLIST_MOD_FILENAME_URL=$(echo $URL_CURSEFORGE/$MODPACK_MODSLIST_MOD_SLUG/files/$MODPACK_MODSLIST_MOD_LATESTFILE)
    MODPACK_MODSLIST_MOD_FILENAME_CONTAIN_SERVER=$(req -sL $MODPACK_MODSLIST_MOD_FILENAME_URL | grep "Server" -B4 | sed -n 1p | grep -oe "[0-9]*")

    if [ -z "$MODPACK_MODSLIST_MOD_FILENAME_CONTAIN_SERVER" ]; then
      echo req $MODPACK_MODSLIST_MOD_FILENAME_URL/download -# -o $MODPACK_MODSLIST_MOD_FILENAME
      req $MODPACK_MODSLIST_MOD_FILENAME_URL/download -# -o $MODPACK_MODSLIST_MOD_FILENAME
    else
      echo req $URL_CURSEFORGE/$MODPACK_MODSLIST_MOD_SLUG/files/$MODPACK_MODSLIST_MOD_FILENAME_CONTAIN_SERVER/download -# -o $MODPACK_MODSLIST_MOD_FILENAME
      req $URL_CURSEFORGE/$MODPACK_MODSLIST_MOD_SLUG/files/$MODPACK_MODSLIST_MOD_FILENAME_CONTAIN_SERVER/download -# -o $MODPACK_MODSLIST_MOD_FILENAME
    fi
  done

  echo -e "done!"
else
  ## Create a temp folder who's gonna hold the files/folders of the modpack
  mkdir -p $SETTINGS_TMP_FOLDER
  [ $SETTINGS_DEBUG = true ] && echo "mkdir -p $SETTINGS_TMP_FOLDER"

  cd $SETTINGS_TMP_FOLDER
  [ $SETTINGS_DEBUG = true ] && echo "cd $SETTINGS_TMP_FOLDER"

  ## Dowloading the "server" archive file from curseforge
  [ $SETTINGS_DEBUG = false ] && echo -e "\nDownloading Server Modpack $MODPACK_SLUG v$MODPACK_VERSION for MC $MODPACK_VERSION_MC by $MODPACK_AUTHOR...\n"

  req $URL_MODPACK/download/$MODPACK_VERSION_SERVER_SLUG/file -# -o modpack.zip
  [ $SETTINGS_DEBUG = true ] && echo req $URL_MODPACK/download/$MODPACK_VERSION_SERVER_SLUG/file -# -o modpack.zip

  ## Extracting the "server" archive file
  [ $SETTINGS_DEBUG = false ] && echo -e "\nExtracting archive...\n"

  unzip -qq modpack.zip
  [ $SETTINGS_DEBUG = true ] && echo unzip -qq modpack.zip

  [ $SETTINGS_PROD = true ] && rm modpack.zip
  [ $SETTINGS_DEBUG = true ] && echo rm modpack.zip

  ## Moving all of the usefull files/folders from $MODPACK_ARCHIVE_CONTENT to the "current" folder
  [ $SETTINGS_DEBUG = false ] && echo -e "\nMoving Modpack stuff...\n"

  MODPACK_ARCHIVEPATH=$(find . -name "mods" | sed -n 1p | sed 's/.mods//')
  [ $SETTINGS_DEBUG = true ] && echo "\$MODPACK_ARCHIVEPATH: $MODPACK_ARCHIVEPATH"

  for el in ./*; do
    el=$(basename $el)

    [[ $MODPACK_ARCHIVE_CONTENT =~ (^|[[:space:]])$el($|[[:space:]]) ]] && mv ./$el ../
    [ $SETTINGS_DEBUG = true ] && echo mv ./$el ../
  done

  for el in $MODPACK_ARCHIVEPATH/*; do
    el=$(basename $el)

    [[ $MODPACK_ARCHIVE_CONTENT =~ (^|[[:space:]])$el($|[[:space:]]) ]] && mv $MODPACK_ARCHIVEPATH/$el ../
    [ $SETTINGS_DEBUG = true ] && echo mv ./$el ../
  done

  ### MinecraftForge Part
  MINECRAFTFORGE_PROVIDED_INSTALLER=$(find . * | grep -P "\.\/([\w\d]*forge[\w\d]*|[\w\d]*server[\w\d]*)-([\d\.]*)-([\d\.]*)-installer\.jar")
  [ $SETTINGS_DEBUG = true ] && echo "\$MINECRAFTFORGE_PROVIDED_INSTALLER: $MINECRAFTFORGE_PROVIDED_INSTALLER"
  MINECRAFTFORGE_PROVIDED_RUNNABLE=$(find . * | grep -P "\.\/([\w\d]*forge[\w\d]*|[\w\d]*server[\w\d]*)-([\d\.]*)-([\d\.]*)-universal\.jar")
  [ $SETTINGS_DEBUG = true ] && echo "\$MINECRAFTFORGE_PROVIDED_RUNNABLE: $MINECRAFTFORGE_PROVIDED_RUNNABLE"
  MINECRAFTFORGE_PROVIDED_RUNNABLE_MC=$(find . -name "minecraft_server.*.jar")
  [ $SETTINGS_DEBUG = true ] && echo "\$MINECRAFTFORGE_PROVIDED_RUNNABLE_MC: $MINECRAFTFORGE_PROVIDED_RUNNABLE_MC"

  ## Searching if forge is provided (installer or universal)
  if [ ! -z "$MINECRAFTFORGE_PROVIDED_INSTALLER" -a "$MINECRAFTFORGE_PROVIDED_INSTALLER" != " " ]; then
    [ $SETTINGS_DEBUG = false ] && echo -e "\nInstalling forge server using the installer jar file provided.\n"

    mv $MINECRAFTFORGE_PROVIDED_INSTALLER ../installer.jar
    [ $SETTINGS_DEBUG = true ] && mv $MINECRAFTFORGE_PROVIDED_INSTALLER ../installer.jar

    cd ..
    [ $SETTINGS_DEBUG = true ] && cd ..

    java -jar installer.jar --installServer
    [ $SETTINGS_DEBUG = true ] && java -jar installer.jar --installServer

    MINECRAFTFORGE_INSTALLED_RUNNABLE=$(find . * | grep -P "\.\/([\w\d]*forge[\w\d]*|[\w\d]*server[\w\d]*)-([\d\.]*)-([\d\.]*)-universal\.jar")
    [ $SETTINGS_DEBUG = true ] && echo "\$MINECRAFTFORGE_INSTALLED_RUNNABLE: $MINECRAFTFORGE_INSTALLED_RUNNABLE"

    mv $MINECRAFTFORGE_INSTALLED_RUNNABLE ./$SERVER_JARFILE
    [ $SETTINGS_DEBUG = true ] && echo mv $MINECRAFTFORGE_INSTALLED_RUNNABLE ./$SERVER_JARFILE

    [ $SETTINGS_DEBUG = false ] && echo -e "\nDeleting installer jar file.\n"

    [ $SETTINGS_PROD = true ] && rm installer.jar
    [ $SETTINGS_DEBUG = true ] && echo rm installer.jar
  elif [ ! -z "$MINECRAFTFORGE_PROVIDED_RUNNABLE" -a "$MINECRAFTFORGE_PROVIDED_RUNNABLE" != " " ]; then
    [ $SETTINGS_DEBUG = false ] && echo -e "\nForge's runnable are provided, no need to download them!\n"
    [ $SETTINGS_DEBUG = false ] && echo -e "\nMoving modpack's server file...\n"

    mv $MINECRAFTFORGE_PROVIDED_RUNNABLE ../$SERVER_JARFILE
    [ $SETTINGS_DEBUG = true ] && echo mv $MINECRAFTFORGE_PROVIDED_RUNNABLE ../$SERVER_JARFILE

    # mv $MINECRAFTFORGE_PROVIDED_RUNNABLE_MC ../
    # [ $SETTINGS_DEBUG = true ] && echo mv $MINECRAFTFORGE_PROVIDED_RUNNABLE_MC ../

    cd ..
    [ $SETTINGS_DEBUG = true ] && echo cd ..
  else
    [ $SETTINGS_DEBUG = false ] && echo -e "\nFinding Forge version through the modpack's files...\n"

    MODPACK_FORGEVERSION_VERSION=$(grep -R "files.minecraftforge.net" --exclude=$SETTINGS_FILENAME | grep -oe "[0-9]\.[0-9][0-9]\.[0-9]\-[0-9][0-9]\.[0-9][0-9]\.[0-9]\.[0-9][0-9][0-9][0-9]" | sed -n 1p)
    [ $SETTINGS_DEBUG = true ] && echo "\$MODPACK_FORGEVERSION_VERSION: $MODPACK_FORGEVERSION_VERSION"

    MODPACK_FORGEVERSION_FILE=$(grep -R "files.minecraftforge.net" --exclude=$SETTINGS_FILENAME | grep -oe "$SETTINGS_TMP_FOLDER/[a-z]*\.[a-z]*")
    [ $SETTINGS_DEBUG = true ] && echo "\$MODPACK_FORGEVERSION_FILE: $MODPACK_FORGEVERSION_FILE"

    if [ ! -z "$MODPACK_FORGEVERSION_VERSION" ]; then
      [ $SETTINGS_DEBUG = false ] && echo -e "\nFound! ($MODPACK_FORGEVERSION_VERSION) at ($MODPACK_FORGEVERSION_FILE)\n"

      FORGE_VERSION=$(echo $MODPACK_FORGEVERSION_VERSION)
      [ $SETTINGS_DEBUG = true ] && echo "\$FORGE_VERSION: $FORGE_VERSION"
    else
      [ $SETTINGS_DEBUG = false ] && echo -e "\nNot found!\n"
      [ $SETTINGS_DEBUG = false ] && echo -e "\nFinding forge's latest version for MC $MODPACK_VERSION_MC\n"

      FORGE_VERSION_1=$(req -sl $URL_MINECRAFTFORGE/index_$MODPACK_VERSION_MC.html | grep -A2 Latest | grep -oe '[1]\.[0-9][0-9]\?\.\?[0-9]\?[0-9]')
      [ $SETTINGS_DEBUG = true ] && echo "\$FORGE_VERSION_1: $FORGE_VERSION_1"
      FORGE_VERSION_2=$(req -sl $URL_MINECRAFTFORGE/index_$MODPACK_VERSION_MC.html | grep -A2 Latest | grep -oe '[0-9][0-9]\.[0-9][0-9]\.[0-9]\?[0-9]\.[0-9][0-9][0-9][0-9]')
      [ $SETTINGS_DEBUG = true ] && echo "\$FORGE_VERSION_2: $FORGE_VERSION_2"
      FORGE_VERSION=$(echo $FORGE_VERSION_1-$FORGE_VERSION_2 | sed 's/ //g')
      [ $SETTINGS_DEBUG = true ] && echo "\$FORGE_VERSION: $FORGE_VERSION"
    fi

    [ $SETTINGS_DEBUG = false ] && echo -e "\nDownloading Forge Version $FORGE_VERSION\n"

    req $URL_MINECRAFTFORGE/$FORGE_VERSION/forge-$FORGE_VERSION-installer.jar -# -o ../installer.jar
    req $URL_MINECRAFTFORGE/$FORGE_VERSION/forge-$FORGE_VERSION-universal.jar -# -o ../$SERVER_JARFILE

    [ $SETTINGS_DEBUG = false ] && echo -e "\nInstalling forge server using the installer jar file.\n"

    cd ..
    [ $SETTINGS_DEBUG = true ] && echo cd ..

    java -jar installer.jar --installServer
    [ $SETTINGS_DEBUG = true ] && echo java -jar installer.jar --installServer

    [ $SETTINGS_DEBUG = false ] && echo -e "\nDeleting installer jar file and cleaning up.\n"

    rm installer.jar
    [ $SETTINGS_DEBUG = true ] && echo rm installer.jar

    rm forge-$FORGE_VERSION-universal.jar
    [ $SETTINGS_DEBUG = true ] && echo rm forge-$FORGE_VERSION-universal.jar
  fi
fi

## Clean the "current" folder, remove all of the "useless" files/folders EXCEPT the actual file (we don't want to delete it!)
[ $SETTINGS_DEBUG = false ] && echo -e "\nCleaning up.\n"

[ $SETTINGS_PROD = true ] && rm -rf $SETTINGS_TMP_FOLDER

[ -f $SETTINGS_FORGE_INSTALL_LOG ] && rm $SETTINGS_FORGE_INSTALL_LOG

[ $SETTINGS_PROD = true ] && find . -name "*.bat" -or -name ".vscode" -not -name $SETTINGS_FILENAME | xargs rm -rf
