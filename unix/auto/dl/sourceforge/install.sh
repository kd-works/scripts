#!/bin/bash

# SourceForge Downloader Script
# Author: Kasai. <me@kasai.moe> (https://kasai.moe)

PROJET_NAME="openofficeorg.mirror"

SETTINGS_DEBUG=false

URL_SOURCEFORGE="https://sourceforge.net"
URL_PROJECT="$URL_SOURCEFORGE/projects/$PROJET_NAME"
URL_PROJECT_FILES="$URL_PROJECT/files"

DOWNLOAD() {
    curl -sSL $1 --create-dirs -o $2 -C -
    [ $SETTINGS_DEBUG = true ] && echo "DOWNLOAD:" curl -sSL $1 --create-dirs -o $2 -C -
}

CREATE_FOLDER() {
    mkdir -p $1
    [ $SETTINGS_DEBUG = true ] && echo "MKDIR:" mkdir -p $1
}

FETCH_FOLDERS() {
    for folder in $1; do
        [ $SETTINGS_DEBUG = true ] && echo "FETCH_FOLDERS"

        [ $SETTINGS_DEBUG = true ] && echo "FOLDERS:" $folder

        PROJET_URL_TMP=$(echo $URL_PROJECT_FILES | sed "s|$URL_SOURCEFORGE||g")
        [ $SETTINGS_DEBUG = true ] && echo "PROJET_URL_TMP:" $PROJET_URL_TMP

        FOLDER=$(echo $folder | tail -c +7 | sed 's/.$//' | sed "s|$PROJET_URL_TMP\/||g")
        [ $SETTINGS_DEBUG = true ] && echo "FOLDER:" $FOLDER

        CREATE_FOLDER $FOLDER

        # fetch current folder for files or folders
        PROJECT_NESTED_FILES=$(curl -sL "$URL_PROJECT_FILES/$FOLDER" | grep -A1 'class="file "' | grep -oe 'href="[a-zA-Z0-9:/.-]*"')
        [ $SETTINGS_DEBUG = true ] && echo "PROJECT_NESTED_FILES:" $PROJECT_NESTED_FILES

        PROJECT_NESTED_FOLDERS=$(curl -sL "$URL_PROJECT_FILES/$FOLDER" | grep -A1 'class="folder "' | grep -oe 'href="[a-zA-Z0-9:/.-]*"')
        [ $SETTINGS_DEBUG = true ] && echo "PROJECT_NESTED_FOLDERS:" $PROJECT_NESTED_FOLDERS

        # start the functions w/o the variables associated
        for file in $PROJECT_NESTED_FILES; do
            FETCH_FILES $file
        done

        for folder in $PROJECT_NESTED_FOLDERS; do
            FETCH_FOLDERS $folder
        done
    done
}

FETCH_FILES() {
    for file in $1; do
        [ $SETTINGS_DEBUG = true ] && echo "FETCH_FILES"

        [ $SETTINGS_DEBUG = true ] && echo "file:" $file

        FILE=$(echo $file | tail -c +7 | sed 's/.$//' | sed 's|/download$||')
        [ $SETTINGS_DEBUG = true ] && echo "FILE:" $FILE

        OUTPUT=$(echo $FILE | sed "s|$URL_PROJECT_FILES\/||g")
        [ $SETTINGS_DEBUG = true ] && echo "OUTPUT:" $OUTPUT

        DOWNLOAD $FILE $OUTPUT
    done
}

PROJECT_FOLDERS=$(curl -sL "$URL_PROJECT_FILES" | grep -A1 'class="folder "' | grep -oe 'href="[a-zA-Z0-9:/.-]*"')
[ $SETTINGS_DEBUG = true ] && echo "PROJECT_FOLDERS:" $PROJECT_FOLDERS

PROJECT_FILES=$(curl -sL "$URL_PROJECT_FILES" | grep -A1 'class="file "' | grep -oe 'href="[a-zA-Z0-9:/.-]*"')
[ $SETTINGS_DEBUG = true ] && echo "PROJECT_FILES:" $PROJECT_FILES

for file in $PROJECT_FILES; do
    FETCH_FILES $file
done

for folder in $PROJECT_FOLDERS; do
    FETCH_FOLDERS $folder
done
