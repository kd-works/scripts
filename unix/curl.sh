#!/bin/bash

echo -ne "\033]2;cURL\007"

echo ""
read -p "   URL : " url
read -p "OUTPUT : " output
echo ""

clear

echo ""
echo -e " * Downloading of \033[31m$output\033[0m..."
echo ""

curl -sSL "$url" -o "$output" -C -

echo ""
echo -e " * Downloading of \033[31m$output\033[0m \033[32mended!\033[0m."
echo ""
