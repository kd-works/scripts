const Downloader = require('./lib/downloader')

const Toonily = require('./sources/toonily')
const ManhwaDotClub = require('./sources/manhwa-club')
const KakaoPage = require('./sources/kakaopage')

const dl = new Downloader({
    website: new KakaoPage(),
    seriesId: '50866481'
})

dl.init()
