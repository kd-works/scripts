const axios = require('axios').default
const { load } = require('cheerio')

class ManhwaDotClub {
    constructor() {
        this._name = 'ManhwaDotClub'

        this._isApi = false
        this._endpoint = 'https://manhwa.club/manhwa'

        this._seriesId = ''
        this._productId = ''
    }

    async _request(type, endpoint, content = '') {
        try {
            const { data } = await axios({
                url: `${this._endpoint}/${endpoint}`,
                method: type,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: content
            })

            return data
        } catch (err) {
            console.error(err)
        }
    }

    async getChapters(seriesId) {
        try {
            this._seriesId = seriesId

            const html = await this._request('GET', seriesId)
            const $ = load(html)
            const tab = []
            const self = this

            $('.wp-manga-chapter').each(function (i, el) {
                const link = $(this).find('a')

                tab.push({
                    id: link.attr('href').split('/').pop().replace('chapter-', ''),
                    title: link.text().replace('\n', '').trim(),
                    link: link.attr('href').replace(self._endpoint, '')
                })
            })

            tab.reverse()

            return tab
        } catch (err) {
            console.error(err)
        }
    }

    async getChapter(product) {
        try {
            this._productId = product.id

            const html = await this._request('GET', product.link)
            const $ = load(html)
            const tab = []

            $('.wp-manga-chapter-img').each((_, el) => {
                const link = el.attribs['data-src'].trim()
                const filename = link.split('/').pop()

                const id = filename.split('.').shift()
                const ext = filename.split('.').pop()

                tab.push({
                    id: product.id,
                    title: product.title,
                    file: {
                        id,
                        ext,
                        link
                    }
                })
            })

            return tab
        } catch (err) {
            console.error(err)
        }
    }
}

module.exports = ManhwaDotClub
