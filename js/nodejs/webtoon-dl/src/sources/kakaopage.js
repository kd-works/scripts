const axios = require('axios').default

class KakaoPage {
    constructor() {
        this._name = 'KakaoPage'

        this._isApi = true
        this._endpoint = 'https://api2-page.kakao.com/api'

        this._seriesId = ''
        this._productId = ''
    }

    async _request(type, endpoint, content = '') {
        try {
            const { data } = await axios({
                url: `${this._endpoint}/${endpoint}`,
                method: type,
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded'
                },
                data: content
            })

            return data
        } catch (err) {
            console.error(err)
        }
    }

    async getChapters(seriesId) {
        try {
            this._seriesId = seriesId

            const { singles } = await this._request('POST', 'v5/store/singles', `seriesid=${seriesId}`)
            const singles2 = []

            singles.forEach(single => {
                singles2.push({
                    id: single.id,
                    title: single.title,
                    url: null
                })
            })
            return singles2
        } catch (err) {
            console.error(err)
        }
    }

    async getChapter(productId) {
        try {
            this._productId = productId

            const { downloadData } = await this._request('POST', 'v1/inven/get_download_data/web', `productId=${productId.id}`)

            return downloadData.members.files
        } catch (err) {
            console.error(err)
        }
    }

    async getDownloadUrl(productId) {
        try {
            const { downloadData } = await this._request('POST', 'v1/inven/get_download_data/web', `productId=${productId.id}`)

            return `${downloadData.members.sAtsServerUrl}${downloadData.members.files[0].secureUrl}`
        } catch (err) {
            console.error(err)
        }
    }
}

module.exports = KakaoPage
