'use strict';

const fs = require('fs')

if (!process.argv[2]) return

fs.readdir('./', (err, files) => {
  files.forEach(file => {
    const name = process.argv[2].split('(x3200)')[0]
    const newFile = file.replace(`${name}- `, '').replace(' (x3200) [FAKKU!]', '')

    fs.rename(file, newFile, err => {
      if (err) throw err
      console.log('renamed!')
    })
  })
})
