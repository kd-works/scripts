function start(el, to = 10e3) {
  const btn = el || document.querySelector('.btn.btn-default')

  btn.dispatchEvent(new MouseEvent('click'))

  const int = setInterval(() => {
    btn.dispatchEvent(new MouseEvent('click'))
  }, 1)

  setTimeout(() => {
    clearInterval(int)
  }, to)
}
