const fs = require('fs');

fs.readdir('./', (err, files) => {
  files.forEach(file => {
    const newFile = file.replace('.jpg', '.png');

    fs.rename(file, newFile, err => {
      if (err) throw err;

      console.log(`File: ${file} successfully renamed to ${newFile}`);
    });
  });
});
