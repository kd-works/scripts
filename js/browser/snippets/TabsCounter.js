// Google Chrome
chrome.tabs.query({ windowType: 'normal' }, tabs => {
  chrome.windows.getAll(null, windows => {
    const pinned = tabs.filter(tab => tab.pinned === true)
    const active = tabs.filter(tab => tab.active === true)

    console.log(`You have ${tabs.length} tab${tabs.length > 1 ? 's' : ''} opened with ${pinned.length} pinned and ${active.length} active${active.length > 1 ? 's' : ''}!`)
    console.log(`You have ${windows.length} windows opened!`)
  })
})

// Mozilla Firefox
browser.tabs.query({}).then(tabs => {
  browser.windows.getAll({ populate: false, windowTypes: ['normal'] }).then(windows => {
    const pinned = tabs.filter(tab => tab.pinned === true)
    const active = tabs.filter(tab => tab.active === true)

    console.log(`You have ${tabs.length} tab${tabs.length > 1 ? 's' : ''} opened with ${pinned.length} pinned and ${active.length} active${active.length > 1 ? 's' : ''}!`)
    console.log(`You have ${windows.length} windows opened!`)
  })
})
