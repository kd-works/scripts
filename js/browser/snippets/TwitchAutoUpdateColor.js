class TwitchAPI {
  constructor() {}

  async request(type, data) {
    return await fetch(`https://gql.twitch.tv/gql?nonce=802b63ce22909ee9cc0218bade68892e`, {
      headers: {
        'Authorization': 'OAuth YOUR-TOKEN-HERE',
        'Client-ID': 'YOUR-TOKEN-HERE',
        'Content-Type': 'text/plain;charset=UTF-8',
        'X-Device-ID': 'YOUR-TOKEN-HERE'
      },
      method: type,
      body: data
    });
  }
}

class Twitch extends TwitchAPI {
  constructor() {
    super();

    this.dataRequest = '[{"operationName":"Chat_UpdateChatColor","variables":{"input":{"color":"__color__"}},"extensions":{"persistedQuery":{"version":1,"sha256Hash":"0371259a74a3db4ff4bf4473d998d8ae8e4f135b20403323691d434f2790e081"}}}]';
  }

  async setColor(color) {
    await super.request('POST', this.dataRequest.replace('__color__', color));
  }
}

const twitch = new Twitch();

const interval = setInterval(() => {
  twitch.setColor('#' + (function co(lor){ return (lor += [0,1,2,3,4,5,6,7,8,9,'a','b','c','d','e','f'][Math.floor(Math.random()*16)]) && (lor.length == 6) ?  lor : co(lor); })(''));
}, 5e3);
