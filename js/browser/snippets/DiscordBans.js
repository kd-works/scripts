async function req(endpoint) {
  try {
    const res = await fetch(`https://discordapp.com/api/v6/guilds/YOUR-GUID-ID-HERE/${endpoint}`, {
      headers: {
        authorization: "TOKEN-HERE",
      },
      method: "GET"
    });

    const data = await res.json()
    return data
  } catch (err) {
    console.error(err)
  }
}

async function count(reason) {
  try {
    const bans = await req('bans')

    const filter = bans.filter(ban => {
      if (ban.reason === null) return false

      if (ban.reason.match(reason)) return ban.reason.match(reason)[0]
      else return false
    })

    console.log(`Il y a ${filter.length}/${bans.length} personnes bannies pour "${reason}" sur ce serveur !`)
  } catch (err) {
    console.error(err)
  }
}

async function list() {
  try {
    const bans = await req('bans')

    console.log(bans)
  } catch (err) {
    console.error(err)
  }
}
