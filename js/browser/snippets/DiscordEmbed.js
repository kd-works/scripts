const token = 'YOUR-TOKEN-HERE';
const channelID = window.location.pathname.split('/').pop();
const embed = { type: 'rich', description: '', color: parseInt(1, 16), title: '', footer: { text: '', icon_url: '' }, image: { url: '' }, video: { url: '' }, thumbnail: { url: '' }, author: { name: '', url: '', icon_url: '' }, timestamp: '', provider: { name: '', url: '' }, field: [{ name: '', value: '', inline: '' }] };

fetch(`https://discordapp.com/api/v6/channels/${channelID}/messages`, {
  headers: { 'Authorization': token, 'Content-Type': 'application/json' },
  method: 'POST',
  body: JSON.stringify({ embed: embed })
});
