class YouTubeMessage {
  constructor(client) {
    this.client = client;
  }

  send(text) {
    const data = {
      context: {
        client: {
          clientName: "WEB_GAMING",
          clientVersion: "1.92",
          hl: "fr",
          gl: "FR",
          experimentIds: [],
          theme: "GAMING"
        },
        capabilities: {},
        request: {
          internalExperimentFlags: [{
            key: "log_foreground_heartbeat_gaming",
            value: "true"
          }, {
            key: "live_chat_use_new_default_filter_mode",
            value: "true"
          }, {
            key: "attach_child_on_gel_web",
            value: "true"
          }, {
            key: "log_js_exceptions_fraction",
            value: "1"
          }, {
            key: "live_chat_replay",
            value: "true"
          }, {
            key: "live_chat_inline_moderation",
            value: "true"
          }, {
            key: "remove_web_visibility_batching",
            value: "true"
          }, {
            key: "third_party_integration",
            value: "true"
          }, {
            key: "live_fresca_v2",
            value: "true"
          }, {
            key: "youtubei_for_web",
            value: "true"
          }, {
            key: "interaction_click_on_gel_web",
            value: "true"
          }, {
            key: "live_chat_top_chat_split",
            value: "0.5"
          }, {
            key: "html5_serverside_pagead_id_sets_cookie",
            value: "true"
          }, {
            key: "log_window_onerror_fraction",
            value: "1"
          }, {
            key: "live_chat_message_sampling_rate",
            value: "4"
          }, {
            key: "retry_web_logging_batches",
            value: "true"
          }, {
            key: "lact_local_listeners",
            value: "true"
          }, {
            key: "debug_forced_promo_id",
            value: ""
          }, {
            key: "use_push_for_desktop_live_chat",
            value: "true"
          }, {
            key: "enable_youtubei_innertube",
            value: "true"
          }, {
            key: "very_optimistically_create_gel_client",
            value: "true"
          }, {
            key: "polymer_page_data_load_refactoring",
            value: "true"
          }, {
            key: "enable_gel_web_client_event_id",
            value: "true"
          }, {
            key: "custom_emoji_upsell",
            value: "true"
          }, {
            key: "interaction_screen_on_gel_web",
            value: "true"
          }, {
            key: "live_chat_flagging_reasons",
            value: "true"
          }, {
            key: "channel_about_page_gadgets",
            value: "true"
          }, {
            key: "enable_docked_chat_messages",
            value: "true"
          }, {
            key: "interaction_logging_on_gel_web",
            value: "true"
          }, {
            key: "live_chat_replay_milliqps_threshold",
            value: "5000"
          }, {
            key: "log_playback_associated_web",
            value: "true"
          }, {
            key: "enable_creator_highlights",
            value: "true"
          }, {
            key: "live_chat_top_chat_window_length_sec",
            value: "4"
          }, {
            key: "live_chat_flash_money_button_on_super_chat_delivery",
            value: "true"
          }, {
            key: "optimistically_create_transport_client",
            value: "true"
          }, {
            key: "web_logging_max_batch",
            value: "100"
          }]
        }
      },
      params: "Q2lNU0lRb1lWVU5aYWtaNlNYUm1ZMHAzV2xaRWJtZzJWVnBXVms5UkVnVXZiR2wyWlJBQkdBUSUzRA==",
      clientMessageId: "CMWi15nY698CFQeYFQod-1oKMA0",
      richMessage: {
        textSegments: [{
          text: "test"
        }]
      }
    }

    fetch(`https://gaming.youtube.com/youtubei/v1/live_chat/send_message?alt=json&key=${this.client.get_INNERTUBE_API_KEY()}`, {
      headers: {
        'authorization': `SAPISIDHASH ${this.client._SAPISIDHASH()}`, // 1547414842_00f672dfbc6093e865dfce26444ccabe7e961c58
        'content-type': 'application/json',
      },
      method: 'POST',
      body: JSON.stringify(data),
    });
  }
}

class YouTubeClient {
  constructor({mail, pass, channel, prefix, cmds=[{name, fn}]}) {
    this.mail = mail;
    this.pass = pass;
    this.channel = channel;
    this.prefix = prefix;
    this.cmds = cmds;
  }

  _oauth() {
    const data = {
      'continue': 'https://www.youtube.com/signin?hl=fr',
      'next': 'https%3A%2F%2Fgaming.youtube.com%2F%3Fpli%3D1&feature=__FEATURE__',
      'app': 'desktop',
      'action_handle_signin': 'true',
      'service': 'youtube',
      'hl': 'fr',
      'f.req': [
        "AEThLlwQBpxUIVqpFE_C41nQkJ9Nk4ym9Clfmku9kt3jO6Y2soDChe2qgIpwpRAxgldOSWMDGULN96DXRsr5kMcME_z5r9YQNr1btIgEj6mXHOzX2xTvNCIN4VxwNdwU9k5d747b6dO51k10jVK0zOinx1z0LW2v4zl7blab_H-GbZ9L5qpUDvEE1_t-8IWc8wTW8HpEzILiHpPz3oWuH4c_bT41Mr0mQSZ5cTCNbJkStT4SOUyEDDnWhANoNW7f4hyKR9YPVKTdo-IchWlZvaPZjJuecAZDt8NcLfA0Ur79pvRM7YGp2aoBnD6X4EVLSen5AlQWLsCml9bnVSzgh1Dw2JgQyZBQq3C8CUQ1nRX8GB7vttlIXarULqXIzDZte79jUWk89dK2CY4E0Xzda845ojEPByDICWlopGI9CWu4tW07n6Z3I8dLpdC0abTYNO7Up9GcabTLwTOsVfSr7mLzAdh0qcsN-Qb-mMPnBh2yFnWkba7WuAk",
        null,
        1,
        null,
        [
          1,
          null,
          null,
          null,
          [
            "184270_k45389",
            null,
            true
          ]
        ],[
          null,
          null,
          [
            2,
            1,
            null,
            1,
            "https://accounts.google.com/ServiceLogin?service=youtube&hl=fr&uilel=3&continue=https%3A%2F%2Fwww.youtube.com%2Fsignin%3Fhl%3Dfr%26next%3Dhttps%253A%252F%252Fgaming.youtube.com%252F%253Fpli%253D1%26feature%3D__FEATURE__%26app%3Ddesktop%26action_handle_signin%3Dtrue&passive=true",
            null,
            [],
            4,
            [],
            "GlifWebSignIn"
          ],
          10,
          [
            null,
            null,
            [],
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            [
              5,
              "424119844901-tilod0e1nm0dt85e1evrdfp3cc3ec01d.apps.googleusercontent.com",
              [
                "https://www.google.com/accounts/OAuthLogin"
              ],
              null,
              null,
              "f1b27e85-0c0a-4aa2-9833-c88d7d73bae7",
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              null,
              5,
              null,
              null,
              [],
              null,
              null,
              null,
              [],
              []
            ],
            null,
            null,
            null,
            null,
            null,
            null,
            [],
            null,
            null,
            null,
            [],
            []
          ],
          null,
          null,
          null,
          true
        ]
      ],
      'bgRequest': [
        "identifier",
        "!bG-lb05CoGjIYc7vIf1EdZLub9EbuecCAAAAYFIAAAAumQNaSPszm4dEc4fQA7qGUnNujG4kNNyvNTxOVOPNhsmB4df05WZw8V-IIst-EkuEEW1PNaRGMH_9E-27c7A4Zvuix43a63-gmvkVFTacPB7l_21S43MX0Rol4SNITFHprSG3T5s0vWtYmIVHchQsp6iZrwzyOnD9R39tbNwfSNnjSf0QVcpxaSoFn68-3TdsrOPUAXvK8JXCiZDL025DPfFb_KSscRUvlymR9On469GCtiz3KGS34b-inZv6_wYDxJwux1wW1cQBLNhg7OKPWIeVVaQKjk99MFF_uA360R8WrDpf30mk8PQf0JQBgN2D1kwN3kRIHFF3ofH8VmhXKdtIF770rcCDKiFkeN9tvl3k0WKW6DCA1BG-0W7uLJXKj5xXYLDI1xs1LCfNUVP3vKlassztlgGnUNir79LN6tWYqHg6f8--7GyzrFWI6bqxbG_e3228dyLV2kBJUmfmof1MnuiW3oTNrR8fIlHn6yyMo-XI-t70I4V7MDoaO2E0DNQzSqqKB61LRSLGHkcxs2ow9RX8aCnPbd-niqpdfLPLGd0Ru_GMxSRcUJaPQ9fJVs0ythm9I5Ysh95jc18XSXAjdi4vMl6XC-309Qh5tZfh3mF1-TbspmoyWsgcflFgFhdLCF0yrLgLCmtSspcFyEmj-fq6rUDf-MqkgNa6P3u1mCBiZvnnghaFf12el5Rs2aX_VXQpkfdsCdZEb6QLi-U3gIZZ-s7ffVuzkxUz0eiOB2oClP0pojahEl4zRTFZ-C1RB_xNttSfjgF3nXj4N_PshZv8LuqoUL47Ha5s8Z8PwKaiPDYVg48PX6aVI4E4ZqrfX4au75oE4AKy1BPXuf7O-T0BGsfU41zU2GTSBWZyOhdag4gXGYHfYmPNXcf9YL-emSCxE4qAzLyshJ8SWxo-3kcw-ZUxriX01T6pXIVKNo5ELujYRXacEa1M-bm84pmyTxsyfSvL1GBLdOUvvQdF31OwMnmCIWVR_Zk592UT-XCZm-zql3GYVsxu0GXPlBkVu-wpIrCGS5lfnpCoPbw3t4nXWPxN5jr_6X2ftg60MYQMY6YOcw_-UrEGbMMC2Ia8rcRB1cb5bG1OiQkBnEN-1yX-U_-qKiCKrOpBwevUIn3zt7L6zwh2hvIP"
      ],
      'bghash': 'JOYx3W5PzoQFRZAOzhrT8YIZJDIx1URDMPi7CeVLUwM&azt=AFoagUWosP7iUQ0SXu2nR7CFZWIe3vSVOA:1547416625547',
      'cookiesDisabled': 'false',
      'deviceinfo': [
        null,
        null,
        null,
        [],
        null,
        "FR",
        null,
        null,
        [],
        "GlifWebSignIn",
        null,
        [
          null,
          null,
          [],
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          null,
          [
            5,
            "424119844901-tilod0e1nm0dt85e1evrdfp3cc3ec01d.apps.googleusercontent.com",
            [
              "https://www.google.com/accounts/OAuthLogin"
            ],
            null,
            null,
            "f1b27e85-0c0a-4aa2-9833-c88d7d73bae7",
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            null,
            5,
            null,
            null,
            [],
            null,
            null,
            null,
            [],
            []
          ],
          null,
          null,
          null,
          null,
          null,
          null,
          [],
          null,
          null,
          null,
          [],
          []
        ]
      ],
      'gmscoreversion': 'undefined',
      'checkConnection': 'youtube:440:1',
      'checkedDomains': 'youtube&pstMsg=1'
    };
    fetch('https://accounts.google.com/_/signin/sl/challenge?hl=fr&_reqid=182683&rt=j', {
      headers: {
        'content-type': 'application/x-www-form-urlencoded;charset=UTF-8',
      },
      method: 'POST',
      body: new URLSearchParams(data.toString())
    });
  }

  _getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
  }

  _loadDynamicScript() {
    const script = document.createElement('script');
    script.src = 'https://cdn.jsdelivr.net/npm/sha1@1.1.1/sha1.min.js';
    document.body.appendChild(script);

    script.onload = () => console.log('sha1.js fully loaded!');
  }

  // https://hitcon.org/2015/CMT/download/day1-b-r0.pdf
  _SAPISIDHASH() {
    return sha1(new Date() + getCookie('SAPISID') + `https://gaming.youtube.com/user/${this.channel}/live`);
  }

  get_INNERTUBE_API_KEY() {
    fetch('https://gaming.youtube.com/user/MinecraftAsterix/live')
      .then(res => res.text())
      .then(text => text.match(/'INNERTUBE_API_KEY': "([a-zA-Z0-9]+)/)[1])
      .then(console.log);
  }

  _onmessage(e) {
    console.log(e);
    const msg = new YouTubeMessage(this, e.data);
  }

  stop() {
    fetch('https://gaming.youtube.com/logout', {
      headers: {
        'cookie': 'cookie',
      },
    });
  }
}

const client = new YouTubeClient({
  mail: 'email@domain.tld',
  pass: 'password',
  channel: 'channel',
  prefix: '!',
  cmds: [{
    name: 'help',
    fn: (bot, msg, author, channel) => {
      debugger;
      const cmds = bot.cmds.map(cmd => cmd.name);
      msg.reply(`Voici les commandes disponnibles: ${cmds}`);
    }
  }]
});
