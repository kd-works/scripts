@echo off

title Ping

set list=(8.8.8.8 8.8.4.4)

:start
  for %%a in %list% do (
    ping %%a -n 1 > nul

    if errorlevel 1 (
      echo.
      echo %%a is UP!
      ping localhost -n 5 > nul
    ) else (
      echo.
      echo %%a is DOWN!
      ping localhost -n 5 > nul
    )
  )
  goto :start
