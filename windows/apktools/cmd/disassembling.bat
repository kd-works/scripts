@echo off

title APKTools - Dissasembling

:start
  set /p file=" * APK FILE : "

  if '%file%' 
  
  for /R "C:\AppServ\fastdl" %f In (*) Do @If %~xf NEq .bz2 (@Move /Y "%~f" "C:\AppSrv\not_bzip">Nul)
  
  java -jar apktool.jar d %1
